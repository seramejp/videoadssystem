<?php
    $url = $_SERVER['HTTP_HOST'];
    $parse = parse_url($url);
    $host = $parse['path'];

    $directory = "";
    if($host == "localhost")
        $directory = "";
    
?>
<!DOCTYPE html>
<html lang="en">
  <?php include_once('parts/head.php') ?>
  <body>
    <?php include_once('parts/nav.php') ?>
    <!-- END nav -->

        <section class="ftco-section bg-light" id="publishers">
			<div class="container-fluid px-4">
				<div class="row justify-content-center mb-5 pb-2">
                    <div class="col-md-8 text-center heading-section ftco-animate">
                        <h2 class="mb-4">Publishers</h2>
                        <p></p>
                    </div>
                </div>	
                <div class="row">

                    <div class="col-md-4 course ftco-animate">
                        <div class="img" style="background-image: url(<?php echo $directory; ?>images/course-6.jpg);"></div>
                        <div class="text pt-4">
                            <!-- <p class="meta d-flex">
                                <span><i class="icon-user mr-2"></i>Mr. Khan</span>
                                <span><i class="icon-table mr-2"></i>10 seats</span>
                                <span><i class="icon-calendar mr-2"></i>4 Years</span>
                            </p> -->
                            <h3><a href="#">Hands-on support to achieve target traffic monetization</a></h3>
                            <p>
                                Our technical team is here to assist publishers not only to drive higher revenues, but also to leave a lasting impression to the hearts and minds of the audience. ThisWebsite engage with audiences across all digital forms. As audience behavior continues to shift towards mobile and portable devices, we provide publishers the solutions to monetize this traffic and offer strategic optimizations to increase revenue.
                            </p>
                            
                        </div>
                    </div>

                    <div class="col-md-4 course ftco-animate">
                        <div class="img" style="background-image: url(<?php echo $directory; ?>images/course-8.jpg);"></div>
                        <div class="text pt-4">
                            <!-- <p class="meta d-flex">
                                <span><i class="icon-user mr-2"></i>Mr. Khan</span>
                                <span><i class="icon-table mr-2"></i>10 seats</span>
                                <span><i class="icon-calendar mr-2"></i>4 Years</span>
                            </p> -->
                            <h3><a href="#">Access to Trusted Brands</a></h3>
                            <p>
                                Spearheaded by our talented Sales team that works directly with partner agencies and brands to manage their digital content. We partner with premium publishers to connect our clients with their target audience. 
                            </p>
                            
                        </div>
                    </div>

                    <div class="col-md-4 course ftco-animate">
                        <div class="img" style="background-image: url(<?php echo $directory; ?>images/course-5.jpg);"></div>
                        <div class="text pt-4">
                            <!-- <p class="meta d-flex">
                                <span><i class="icon-user mr-2"></i>Mr. Khan</span>
                                <span><i class="icon-table mr-2"></i>10 seats</span>
                                <span><i class="icon-calendar mr-2"></i>4 Years</span>
                            </p> -->
                            <h3><a href="#">Easy Integration</a></h3>
                            <p>
                                ThisWebsite integration is very simple, just add one line of code and you're done, no need for technical expertise. Our technical experts are on Stand-by to help you with anything, just in case. 
                            </p>
                            
                        </div>
                    </div>
                </div>

            </div>
        </section>


        <?php include_once('parts/contact.php') ?>
		
        <?php include_once('parts/footer.php') ?>

        <?php include_once('parts/scripts.php') ?>
    
    </body>
</html>