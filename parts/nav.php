<nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
            
        <div class="container d-flex align-items-center px-4">
                <div class="col-md-4 d-flex align-items-center py-4">
                        <img src="images/site_logo2.png?_<?= time() ?>" alt="Site Logo">
                        <a class="navbar-brand" href="">ThisWebsite</a>
                    </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>
        
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a href="" class="nav-link pl-0">Home</a></li>
                <li class="nav-item"><a href="advertisers.php" class="nav-link">Advertisers</a></li>
                <li class="nav-item"><a href="publishers.php" class="nav-link">Publishers</a></li>
                <li class="nav-item"><a href="#contact" class="nav-link">Contact</a></li>
            </ul>
        </div>
        </div>

</nav>