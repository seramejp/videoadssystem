<?php
        if(isset($_POST['submit'])){
            #$to = "jp@offertoro.com"; 
            $to = "";
            $from = $_POST['email']; // this is the sender's Email address
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $subject = "ThisWebsite User - " . $from . ", an ".$_POST['path']." sent us a message";
            $message = "From: ". $firstname . " " . $lastname . "\nEmail: " . $from . "\n\n" . $_POST['message'];
            $headers = "From:" . $from;

            #mail($to,$subject,$message,$headers);
            $is_show_ty = "true";
            
        }
?>

        <section id="contact" class="ftco-section ftco-consult ftco-no-pt ftco-no-pb" style="background-image: url(images/bg_5.jpg);" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-md-6 py-5 px-md-5">
                        <div class="py-md-5">
                            <div class="heading-section heading-section-white ftco-animate mb-5">
                                <h2 class="mb-4">Contact Us</h2>
                                <p>
                                    Excited to get started? We are one message away. 
                                </p>
                            </div>
                            
                            <form action="" class="appointment-form ftco-animate" method="POST" enctype="multipart/form-data">
                                <div class="d-md-flex">
                                    <div class="form-group">
                                        <input name="firstname" type="text" class="form-control" placeholder="First Name" required>
                                    </div>
                                    <div class="form-group ml-md-4">
                                        <input name="lastname" type="text" class="form-control" placeholder="Last Name" required>
                                    </div>
                                    
                                </div>
                                
                                <div class="d-md-flex">
                                    <div class="form-group">
                                        <div class="form-field">
                                            <div class="select-wrap">
                                                    <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                                    <select name="path" id="" class="form-control">
                                                        <option value="0">Select Your Path</option>
                                                        <option value="publisher">Publisher</option>
                                                        <option value="advertiser">Advertiser</option>
                                                    </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-md-4">
                                        <input name="email" type="email" class="form-control" placeholder="Email" required>
                                    </div>
                                </div>
                                
                                <div class="d-md-flex">
                                    <div class="form-group">
                                        <textarea name="message" id="" cols="30" rows="2" class="form-control" placeholder="Message" required></textarea>
                                    </div>

                                    <div class="form-group ml-md-4">
                                        <input name="submit" type="submit" value="Send Message" class="btn btn-primary py-3 px-4">
                                    </div>
                                </div>
                                
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Modal -->
        <div id="myModal_ty" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"> Message Sent!</h4>
                        <button type="button" class="close-modal" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="modal-body">
                        <p>Your message was sent!</p>
                        <br>
                        <p>Thank you for contacting us <?php echo (isset($_POST['submit']) ? ", ".$_POST['firstname']."." : "."); ?> </p>
                        <br>
                        <p>We will get back to you as soon as possible.</p>
                    </div>
                    <div class="modal-footer">
                        
                        <div class="col-md-3">
                            <button type="button" class="btn btn-lg btn-primary btn_prim" data-dismiss="modal"> Okay</button>
                        </div>
                        
                    </div>
                </div>

            </div>
        </div>

        <script>
            var is_show_ty = <?php  echo (isset($is_show_ty) ? $is_show_ty : "false" ); ?>;
            <?php unset($_POST); $_POST = []; ?>
        </script>