        <footer id="footer_sct" class="ftco-footer ftco-bg-dark ftco-section">
            <div class="container">
                <div class="row mb-5"> <!-- mb-5 -->
                    
                    <div class="col-md-6 col-lg-3">
                        <div class="ftco-footer-widget mb-5">
                            <h2 class="ftco-heading-2">Have Questions?</h2>
                            <div class="block-23 mb-3">
                            <ul>
                                <li><a href="mailto:serame.jp@gmail.com?Subject=Hi!%20I%20have%20a%20Question"><span class="icon icon-envelope"></span><span class="text">serame.jp@gmail.com</span></a></li>
                            </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6">
                        <div class="ftco-footer-widget mb-5 ml-md-4">
                            <h2 class="ftco-heading-2">Links</h2>
                            <ul class="list-unstyled">
                                <li>
                                    <a href="">Home</a> &emsp; | &emsp;
                                    <a href="advertisers.php">Advertisers</a> &emsp; | &emsp;
                                    <a href="publishers.php">Publishers</a> &emsp; | &emsp;
                                    <a href="#contact">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    
                
                </div>
                <!-- <div class="row">
                    <div class="col-md-12 text-left">
                        <p>
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> 
                            All rights reserved | This template is made with 
                            <i class="icon-heart" aria-hidden="true"></i> by 
                            <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        </p>
                    </div>
                </div> -->
            </div>
        </footer>