<!DOCTYPE html>
<html lang="en">
  <?php include_once('parts/head.php') ?>
  <body>
        <?php include_once('parts/nav.php') ?>
        <!-- END nav -->
    
        <section class="home-slider owl-carousel">
            <div class="slider-item" style="background-image:url(images/bg_1.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row no-gutters slider-text align-items-center justify-content-start" data-scrollax-parent="true">
                        <div class="col-md-6 ftco-animate">
                            <h1 class="mb-4">Monetize your content with <span>ThisWebsite</span></h1>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, fuga.</p> -->
                            <p><a href="#contact" class="btn btn-primary px-4 py-3 mt-3">Contact Us</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="slider-item" style="background-image:url(images/bg_2.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row no-gutters slider-text align-items-center justify-content-start" data-scrollax-parent="true">
                        <div class="col-md-6 ftco-animate">
                            <h1 class="mb-4">Expose your videos to your target audience<span>ThisWebsite</span></h1>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem tenetur quas odit?</p> -->
                            <p><a href="#contact" class="btn btn-primary px-4 py-3 mt-3">Contact Us</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

		<section class="ftco-section ftco-no-pt ftc-no-pb" id="about">
			<div class="container">
				<div class="row d-flex">
					<div class="col-md-5 order-md-last wrap-about wrap-about d-flex align-items-stretch">
                        <div class="img" style="background-image: url(images/bg_4.jpg); border"></div>
					</div>
					<div class="col-md-7 wrap-about py-5 pr-md-4 ftco-animate">
                        <h2 class="mb-4">About Us</h2>
						<p>We are the digital advertising solution that will bring your brand to the next level! We help YOU leverage the power of video advertising for your target audience in order to achieve quality results.</p>
						<div class="row mt-5">
                            <div class="col-lg-6">
								<div class="services-2 d-flex">
                                <div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-education"></span></div>
									<div class="text pl-3">
										<h3>Expose Your Brand</h3>
										<p>ThisWebsite presents your ads across sites you believe in.</p>
									</div>
								</div>
                            </div>

                            <div class="col-lg-6">
								<div class="services-2 d-flex">
									<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-books"></span></div>
									<div class="text pl-3">
										<h3>Platforms</h3>
										<p>We are ready for all platforms. Achieve next level results by engaging customers with your captivating videos.</p>
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="services-2 d-flex">
									<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-diploma"></span></div>
									<div class="text pl-3">
										<h3>Powerful AI</h3>
										<p>We use cutting-edge technology to help you achieve the best campaign performance. Our intelligent system filters bots and does non-invasive fraud detection - a technology at par with the giants.</p>
									</div>
								</div>
                            </div>
                            
                            <div class="col-lg-6">
								<div class="services-2 d-flex">
									<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-kids"></span></div>
									<div class="text pl-3">
										<h3>Profiling</h3>
										<p>We have varied customers targeting audiences from all walks of life. If you are a Media-buyer, an Advertiser, or a Publisher feel free to contact us, we can help you grow and nurture the things you love!</p>
									</div>
								</div>
                            </div>
                            
                            <div class="col-lg-6">
								<div class="services-2 d-flex">
									<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-security"></span></div>
									<div class="text pl-3">
										<h3>Targeted Audience</h3>
										<p>ThisWebsite provides a variety of targeting capabilities to effectively reach your target audiences. Our vast global database, together with our top-notch AI will provide you the target audience you so well deserve.</p>
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="services-2 d-flex">
									<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-jigsaw"></span></div>
									<div class="text pl-3">
										<h3>Technical Support</h3>
										<p>Our technical staff is equipped with top-notch technical expertise to provide you the best support there is. Let us know your ideas and we will make it happen!</p>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>

        <?php include_once('parts/contact.php'); ?>
		
        <?php include_once('parts/footer.php'); ?>

        <?php include_once('parts/scripts.php'); ?>


    
    </body>
</html>