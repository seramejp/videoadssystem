<?php
    $url = $_SERVER['HTTP_HOST'];
    $parse = parse_url($url);
    $host = $parse['path'];

    $directory = "";
    if($host == "localhost")
        $directory = "";
    
?>
<!DOCTYPE html>
<html lang="en">
  <?php include_once('parts/head.php') ?>
  <body>
            

    <?php include_once('parts/nav.php') ?>
    <!-- END nav -->
    

        <section class="ftco-section" id="advertisers">
			<div class="container-fluid px-4">
				<div class="row justify-content-center mb-5 pb-2">
                    <div class="col-md-8 text-center heading-section ftco-animate">
                        <h2 class="mb-4">Advertisers</h2>
                        <p></p>
                    </div>
                </div>	

                <div class="row">
                    <div class="col-md-6 course ftco-animate">
                        <div class="img" style="background-image: url(<?php echo $directory; ?>images/course-1.jpg);"></div>
                        <div class="text pt-4">
                            <!-- <p class="meta d-flex">
                                <span><i class="icon-user mr-2"></i>Mr. Khan</span>
                                <span><i class="icon-table mr-2"></i>10 seats</span>
                                <span><i class="icon-calendar mr-2"></i>4 Years</span>
                            </p> -->
                            <h3><a href="#">Audience</a></h3>
                            <p>
                                With an audience ranging from the young to the young-at-heart, from school boys to experienced professionals, regardless of gender, vegans or meat-lovers, gamers, moms, you name it - ThisWebsite has mastered the ways to communicate with any type of audience. ThisWebsite is the perfect solution for your advertising needs. 
                            </p>
                            
                        </div>
                    </div>

                    <div class="col-md-6 course ftco-animate">
                        <div class="img" style="background-image: url(<?php echo $directory; ?>images/course-2.jpg);"></div>
                        <div class="text pt-4">
                            <!-- <p class="meta d-flex">
                                <span><i class="icon-user mr-2"></i>Mr. Khan</span>
                                <span><i class="icon-table mr-2"></i>10 seats</span>
                                <span><i class="icon-calendar mr-2"></i>4 Years</span>
                            </p> -->
                            <h3><a href="#">Targeting Strategies</a></h3>
                            <p>
                            ThisWebsite strategically connects with partners to achieve a better understanding of the advertising landscape, making sure we are the forerunners in this technology. 
                            Our team is scientifically equipped in creating specialized advertising technologies to reach and engage your target niche. 
                            These specialized advertising technologies include: Predictive Targeting, Fraud detection, Geo-fencing and Location targeting, Bots and Spam prevention, Device Lookback
                            </p>
                            
                        </div>
                    </div>
                    <div class="col-md-6 course ftco-animate">
                        <div class="img" style="background-image: url(<?php echo $directory; ?>images/course-3.jpg);"></div>
                        <div class="text pt-4">
                            <!-- <p class="meta d-flex">
                                <span><i class="icon-user mr-2"></i>Mr. Khan</span>
                                <span><i class="icon-table mr-2"></i>10 seats</span>
                                <span><i class="icon-calendar mr-2"></i>4 Years</span>
                            </p> -->
                            <h3><a href="#">Premium Media, Trusted Brands</a></h3>
                            <p>
                                Ads run on trusted titles are 85% more likely to drive customer acquisition. 
                                Advertisers will benefit from our premium media partnerships and trusted brands, elevating the trending experience. 
                                ThisWebsite ensures all content is presented in a safe and engaging environment.
                            </p>
                            
                        </div>
                    </div>
                    <div class="col-md-6 course ftco-animate">
                        <div class="img" style="background-image: url(<?php echo $directory; ?>images/course-4.jpg);"></div>
                        <div class="text pt-4">
                            <!-- <p class="meta d-flex">
                                <span><i class="icon-user mr-2"></i>Mr. Khan</span>
                                <span><i class="icon-table mr-2"></i>10 seats</span>
                                <span><i class="icon-calendar mr-2"></i>4 Years</span>
                            </p> -->
                            <h3><a href="#">Technical Crew</a></h3>
                            <p>
                                You are our focus. Client satisfaction is of the utmost importance to ThisWebsite. We at ThisWebsite are committed to deliver proactive efficient and friendly service to every client - big or small. This technical crew strives to continuously improve campaign performance as well as bring the client proactive suggestions for optimizations.
                            </p>
                            
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <?php include_once('parts/contact.php') ?>
		
        <?php include_once('parts/footer.php') ?>

        <?php include_once('parts/scripts.php') ?>


    
    </body>
</html>